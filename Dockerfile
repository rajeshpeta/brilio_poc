FROM tomcat
MAINTAINER Rajesh Peta <rajeshpeta67@gmail.com>
RUN rm -rf /usr/local/tomcat/webapps/*
COPY target/*.war /usr/local/tomcat/webapps/
RUN sed -i 's/port="8080"/port="8091"/' /usr/local/tomcat/conf/server.xml
EXPOSE 8090
CMD ["catalina.sh", "run"]
